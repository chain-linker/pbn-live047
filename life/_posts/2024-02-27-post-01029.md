---
layout: post
title: "[수원 분양] 매교역 팰루시드 분양가, 평면도, 분양정보"
toc: true
---

 수원 매교역 팰루시드 분양가와 일정, 신청 사권 등에 대해 알아보겠습니다.
 

 12월 26일부터 청약 신청할 성명 있는 매교역 팰루시드는 관계 경기도 수원시 권선구 세류동 817-72번지에 위치해있습니다. 총 32개 동으로 2,178세대 규모의 대단지 아파트입니다. 최고 15층 높이로 이루어져있으며 48~101㎡의 다양한 안정화 타입이 있는데 각각의 분양가와 일정에 대해 정리해 보았습니다.
 

 

 

## 1. 청약일정
 

 모집공고: 2023년 12월 15일 금요일
 특별공급 2023년 12월 26일 화요일
 1순위 2023년 12월 27일 수요일
 2순위 2023년 12월 28일 목요일
 당첨자 발표: 2024년1월 5일 금요일
계약: 2023년1월 19일 - 25일
입주: 2026년 8월 예정
 

 

 

 

### 2. 청약조건
 

 입주자 모집 공고일 기준 만 19세 극점 수원시, 경기도, 서울, 인천 거주자라면 청약 신청할 길운 있습니다. 청약통장 입단 12개월 이상, 지역/면적별 예치금 기준을 만족해야 합니다.해야합니다.

 

 <예치금 기준>
 경기도: 85㎡ 이하 200만 기망 / 102㎡ 이다음 300만 원
 서울: 85㎡ 휘하 300만 의망 / 102㎡ 휘하 600만 원
 인천: 85㎡ 뒤끝 250만 가망 / 102㎡ 안쪽 400만 원
 

 

 <제한>
 전매 제한: 1년(~25년 1월)
 거주의무: x
 재당첨 제한: 투과/청약과열 토양 1순위만 5년 제한
 

 비규제지역에서 공급하는 분양가상한제 미적용 아파트로 전매제한은 발표일 준거 1년이며 거주의무는 없습니다.
 

 

 <당첨자 선정>
 85㎡ 이하: 가점 40% 추첨 60%
 85㎡ 초과: 추첨 100%
 같은 위치 기미 병혁 있을 경우, 수원 거주자에게 미리미리 공급
 

 

#### 3. 입지 환경
 

 

 

 <교통>
분당선 매교역까지 200m 떨어져 있어 도보로 1분 거리인 초역세권입니다. 1.5km 떨어진 수원역에서는 1호선과 KTX를 이용할 목숨 있으며 나중 GTX-C 노선과 수원 1호선 트램이 예정되어 있는 호재가 있습니다. 쿼드러플 역세권으로 발전할 가능성이 있습니다.
 

 <편의시설>
홈플러스와 롯데마트가 가까이에 있고 롯데백화점이 인근에 있습니다. 수원중앙병원도 근처에 있으며 수원역 상권을 누릴 생령 있어 생활하기에 편리할 것으로 보입니다. 수원시립중앙도서관과 수원시청 등의 공공시설도 있어 주위 인프라가 걸핏하면 갖추어져 있습니다.
 

 <자연환경>
 세계문화유산인 수원화성이 근처에 있고 팔달공원과 올림픽공원 등이 언제나없이 조성되어 있습니다. 수원천도 있어서 산책하기에 좋을 것 같습니다.

<학군>
 권선초까지는 도포 4분 거리로 가깝지만 길을 건너야 한다는 아쉬운 부분이 있습니다. 애오라지 인근에 수원중/고가 있고 성균광대와 아주대도 인접해 있어 인근의 교수 환경을 누릴 핵심 있습니다.
 

 

 

#### 4. 분양가, 분양정보, 평면도
 48㎡ 부터 101㎡까지 다양한 평형대가 있습니다.
 

 

 48A 타입의 분양가: 5억 [매교역 팰루시드](https://quarrel-sleepy.com/life/post-00090.html) 7900 만원~ 6억 1600만 원
 48B 타입의 분양가: 5억 9300만 기망 ~  6억 2500만 원
 59B 타입의 분양가: 6억 9200만 기망 ~  7억 3600만 원
59C 타입의 분양가: 6억 9400만 기대 ~  7억 3800만 원
71A 타입의 분양가: 7억 7100만 기망 ~  8억 2000만 원
71B 타입의 분양가: 7억 7000만 기망 ~  8억 1000만 원
84A 타입의 분양가: 8억 5400만 의망 ~  8억 9900만 원
84B 타입의 분양가: 8억 5200만 축원 ~  8억 9700만 원
101 타입의 분양가: 11억 6200만 희원 ~  11억 9800만 원
 

 

 계약금: 분양가의 10%
중도금: 60% (이자후불제)
잔금: 30%
 

 

 <모집공고문>

 

 

 

 

 

 48A / B
 22 / 9세대
 

 

 

 

 59B / C
 62 / 108세대

 

 

 71A / B타입
 196 / 45세대

 

 

 84A / B타입
 676 / 58세대

 

 

 101 타입
 58세대

 

 

 

 

#### 5. 처지 시세
 근처 분양가
2020년에 분양한 매교역 푸르지오 SK뷰 59 타입 분양가는 4억 2~3천만 원, 74 타입 분양가는 5억 3~6천만 원, 99 타입 분양가는 7억 4천만 광명 정도였습니다.
 본년 2023년 2월에 분양한 수원성중흥 S클래스 59 타입분양가는 5억 1600~5800만 원, 75 타입은 6억 1210~6200만 원, 84 타입은 7억 700~5900만 원, 106 타입은 8억 2760~8260만 원이었습니다. 수원성중흥 S클래스는 푸르지오 SK뷰에 비해서 매교역에서 어지간히 더한층 떨어진, 입지가 비교적 좋지 않았지만 공사비 인상으로 인해 분양가가 더욱 높았습니다.
주변 실거래가
 2022년 7월에 입주를 시작한 매교역 푸르지오 SK뷰는 23년 11월 84 타입이 9억 1,000만 원에 거래됐습니다. 59 타입은 11월 6억 1,000만 원에 거래되었습니다. 이번에 분양을 하는 매교역 팰루시드의 확장비를 포함한 분양가를 비교하면 비슷한 수준입니다.
 

 

 

 삼성디지털시티 수원사업장과 가까운 직주 근접의 단지입니다. 매교역까지 1분이면 갈 길운 있는 초역세권으로 입지가 좋아 보입니다. 다만, 인접해 있는 푸르지오 SK뷰의 세상영문 수준으로 안전마진은 대부분 없습니다. 실거주를 원하시더라도 청약을 곧 신청하기보다는 푸르지오 SK뷰의 매물과 비교해 보는 편이 나아 보입니다. 더군다나 푸르지오SK뷰는 한갓 내에 초중고를 품고 있어 아이를 둔 세대는 푸르지오가 더 좋아 보이기도 합니다.
 

 매교역은 위기 재개발이 진행되고 있는 지역으로 앞으로 일층 좋아질 전망입니다. 뿐만 아니라 수원역에 교제 호재가 몰려 있어 시세차익을 위해 청약하시는 분들도 많아 경쟁률이 많이 높을 것으로 보입니다.
