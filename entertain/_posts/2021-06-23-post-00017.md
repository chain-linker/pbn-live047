---
layout: post
title: "넷플릭스 6월의 신작 드라마편"
toc: true
---

 벌써 여름이 성큼 다가오는 만큼 집에서 시원한 에어컨 바람 쐬며 드라마 정주행 하는 분들이 많아지셨는데요. 오늘은 새로운 드라마를 찾고 계신 분들을 위해 넷플릭스 6월의 신작 드라마편을 소개시켜드리려고 하는데 좋은 정보가 되셨음 좋겠네요~
 

 

 

 공개일 : 6월 4일
장르 : 드라마, 판타지
등급 : 15세 관람가
출연 : 논소 아노지, 크리스천 콘버리, 아딜 액터, 스테파니아 러비 오언, 다니아 라미레스, 알리자 벨라니 등
제작 : 짐 미클
런닝타임 : 4~50분
시놉시스
반은 인간이고 반은 사슴. 하지만 너무도 사랑스러운 소년. 그 아이가 종말 이후의 세상을 가로질러 위험한 모험을 시작한다. 퉁명스러운 보호자와 함께, 어딘가 있을 새로운 시작을 찾아.
 

 

 

 

 

 

 공개일 : 6월 17일
장르 : 드라마, 한국, 코미디
등급 : 15세 관람가
출연 : 조정석, 유연석, 정경호, 김대명, 전미도, 김해숙, 김갑수, 정문성 등

제작 : 신원호, 이우정
런닝타임 : 1시간 2~30분
시놉시스
탄생과 죽음이 공존하는, 인생의 축소판이라고 불리는 병원에서 특별한 하루하루를 살아가는 사람들의 이야기를 그린 드라마
 

 

 

 

 

 

 공개일 : 6월 17일
장르 : 드라마, 공포, 미스터리
등급 : 청소년 관람불가
출연 : 그뷔드룬 에이피외르드, 이리스 타니아 플리겐링그, 알리에테 옵헤임, 잉그바르 시귀르손, 소르스테이든 바크만, 솔베이그 아르드나르스도티르, 비외르든 토르스 등
제작 : 바흘타사르 코르마우퀴르
런닝타임 : 45~50분
시놉시스
빙저 화산 카틀라가 폭발하면서 재앙이 닥친 마을. 그 후 1년, 빙하에서 수수께끼의 존재가 등장하면서 주민들의 삶이 뒤집힌다. 화산재를 뒤집어쓴 채 알몸으로 나타난 그들의 정체가 무엇인가.
 

 

 

 

 

 

 공개일 : 6월 19일
장르 : 웹툰 원작 한국 드라마, 로맨스, 한국 드라마
등급 : 청소년 관람불가
출연 : 송강, 한소희, 채종협, 이열음, 양혜지 등
제작 : 김가람, 장지연
런닝타임 : 알 수 없음
시놉시스
사랑을 믿지 못하게 됐다. 그렇다고 연애마저 포기해야 할까. 그런 미대생 앞에 치명적인 매력을 지닌 같은 과 학생이 나타나 유혹의 몸짓을 던진다. 그들 사이에 파트너 같은 관계가 시작된다.
 

 

 

 

 공개일 : 6월 17일
장르 : 드라마, 좀비, 공포
등급 : 청소년 관람불가
출연 : 제이미 킹, 저스틴 추 케리, 크리스틴 리, 살 벨레스 주니어, 켈시 플라워, 조이 말릿, 에리카 하우 등
제작 : 칼 셰이퍼, 존 하이엄스
런닝타임 : 35~50분
시놉시스
좀비 대재앙이 세상을 뒤덮은 암흑의 시대, 인류는 절멸의 위기에 처하고, 얼마 안 되는 생존자들은 서로 힘을 모은다. 끝까지 살아남아 사랑하는 이들에게 돌아가기 위해.
 

 

 

 

 

 공개일 : 6월 11일
장르 : 드라마, 액션, 추리
등급 : 15세 관람가
출연 : 오마르 시, 뤼디빈 사니에, 클로틸드 엠, 니콜 가르시아, 에르베 피에르, 수피안 게라브, 앙투안 고이 등
제작 : 조지 케이
런닝타임 : 4~50분
시놉시스
뤼팽이 부활할 시간이 왔다. 25년 전 아버지를 억울한 죽음으로 몰아넣은 프랑스 최고의 재벌 가문. 그들에게 정의의 철퇴를 내리려 청년 아산은 오랫동안 동경했던 영웅으로 다시 태어난다. 수많은 이의 마음을 훔쳤던 그 괴도 신사로.
 

 

 

 

 

 

 [영화 다시보기](https://bearssteady.gq/entertain/post-00002.html) 

 

 

 공개일 : 6월 11일
장르 : 범죄, 애니메이션, 호러, SF&판타지

등급 : 청소년 관람불가
출연 : 셰이 미첼, 라이자 소베라노, 존 존 브리오니스, 대런 크리스, 매니 자신토, 단테 바스코, 니콜 셰르징거 등

제작 : 제이 올리바
런닝타임
마닐라 곳곳에서 범죄 사건이 발생한다. 어두운 초자연적 세력들이 개입된 일이다. 도시의 평화를 지킬 유일한 희망은 알렉산드라 트레세. 하지만 강한 그녀 앞에 더 큰 폭풍이 불어닥치려 한다.

이번 넷플릭스 6월의 신작 드라마편은 특히 기존에 있던 드라마들이 새로운 시즌으로 찾아와 저희의 눈도장을 제대로 찍은 것 같은데요. 다음에도 더욱 재미있는 정보로 찾아오도록 하겠습니다~ 무더위 조심하세요!

## '일상' 카테고리의 다른 글
