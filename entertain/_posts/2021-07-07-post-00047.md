---
layout: post
title: "6월 넷플릭스 영화 '어웨이크' 예고편으로 영어공부"
toc: true
---

 2021년 6월 9일 SF 스릴러 영화, 어웨이크(Awake)를 넥플릭스에서 시작합니다. 어웨이크 뜻은 영어단어 Awake의 안출 그대로인 "잠들지 못하는", "깨어있는"입니다. 지구에 무슨일이 있었는지, 인공위성이 추락하고 자동차가 고장납니다. 바꿔 말하면 너 이후에 사람들이 잠을 못 자게 되는데요. 사람이 잠을 못자면 완전 예민해지죠? 우리나라에는 이런즉 현상이 매일매일 일어나지 않나 싶습니다. 시고로 상황에서는 야근과 철야가 익숙해진 K-회사생활이 정답일까요? ㄷㄷㄷㄷㄷㄷ

 

 하여간에 지구에 일어난 수모 문제가 영화 버드박스(Bird box)처럼 억지스럽지 않았으면 좋겠습니다. 그럼 미리미리 영화 정보부터 알아보고 어웨이크 예고편에 나온 영어표현, 어휘, 문법 등으로 영어공부 해볼까요?

## 영화 어웨이크(Awake) 정보
 

 전 세계에 뭔가 재앙이 들이 닥치고요. 인공위성이 땅으로 추락합니다. 또한 자동차를 포함하여 모든 전자기기들이 먹통이 되버립니다. 필야 태양에서 기미 뿜어내는 EMP 때문일지도 모르겠네요. 더불어서 사람들이 잠에 들지 못하게 됩니다. 잠을 못자니까 예민해지기 시작하죠.
 

 

 사람이 잠을 못 자면 생기는 증상 첫 번째는 "방향감각 상실"입니다. 영어로 "Disorientation"입니다. 아낙네 그래도 전자기기가 남김없이 먹통인데 혼미해질 요체 밖에요. 스마트폰이나 콘솔 게임기가 엄연히 먹통이 된다고 생각해보세요. 끔찍하겠죠? ㄷㄷㄷㄷㄷㄷㄷ
 

 

 잠을 못 자면 생기는 2번째 증상이 닁큼 환각 "Hallucinations"입니다. 정신도 혼미하고 밤에 헛것이 보이면 보약을 챙겨먹어야 하는데 말입니다.
 

 

 잠을 못 자면 막바지 히스테리(Hysteria)가 터지죠? 우리 회사 부장님은 얼마나 못 주무시길레 히스테리를 부리시는 걸까요? 시고로 상황에서도 우리나라 사람들은 모씨 정확히 버티지 않을까 싶어요.
 

 

 유일하게 잠을 항상 성명 있는 자식새끼 마틸다 역은 배우 "아리아나 그린블랫"이 연기합니다. 군 옆에 마틸다 엄마는 "지나 로드리게스"가 연기하고요.
 

 오잉! 아라아나 그린블랫 필모그라피를 보니까 어벤져스: 인피니티 워에서도 나왔네요!!!!!! 어린 가모라 역할을 했던 배우입니다.
 

 얼마전 넷플릭스에서 개봉된 스토어웨이의 밀항자로 나왔던 셰미어 앤더슨입니다. 최근 넷플릭스에서 두각을 굉장히 보여주고 있네요.
 

 

 제니퍼 제이슨 리 이분은 근래 악역으로 주목받는 배우입니다. 얼마전 우먼 인계 윈도우에서도 출연한 바 있습니다. 어웨이크에서도 마틸다를 대상으로 실험을 하자는 박사로 나오죠.
 

 여기까지 짧게 영화 정보를 알아봤어요. 아래에서는 어웨이크 예고편을 보면서 듣기 연습도 해보고 모르는 단어나, 표현들이 있는지 살펴보시죠.
 

 www.youtube.com/watch?v=_hS___u12bw
 

 -Why don't you get some sleep?
 얼마나 안자고?
 

 -I'm not tired.
 그다지 처속 피곤해요.
 

 Look at all the shooting stars.
 유성이 무진히 떨어져요.
 

 Those are satellites, Noah.
 저거 대다수 위성이야, 노아

 

 I don't know what the hell happened.
 무슨 영문인지 모르겠네요.
 [영화](https://translate-scattered.tk/entertain/post-00004.html) 

 Power's out everywhere.
 전기가 몽땅 끊겼어요.
 

 Cars too.
 차들도 멈췄죠.
 

 Fifteen hours ago, something happened.
 15시간 전 무슨일이 벌어졌는데.
 

 We don't know what caused it or why it occurred.
 뭐가 원인이고 뭐가 문제인지 몰라요.
 

 But what we do know is that none of us can sleep.
 어렵사리 아는 건 아무도 잠을 못 잔다는 거죠.
 

 After 48 hours of no sleep, there's a loss of critical thinking.
 48시간 공상 못 자면, 비판전 사고가 흐려져요.
 

 It's gonna be total chaos.
 아수라장이 될 겁니다.
 

 Get in.
 타요.
 

 But what about after five or six days?
 5, 6일이 지나면요?
 

 We're all gonna die
 우린 대다수 죽을 거예요.
 

 if we don't solve this.
 해결책을 못 찾으면요.
 

 Let's go! Right now!
 출발해야 해, 당장!
 

 -Move!
 출발해요!
 -I'm trying!
 하려고 하잖아요!!
 

 Hands up! Get out!
 기공 들고 나와!
 

 -Don't shoot. 쏘지 마세요.
 -Hands up. 손들어

 

 I can sleep. 전 곧이어 행운 있어요.
 

 We need to get your daughter to Murphy. She could be key.
 이이 딸을 박사님께 데려가죠. 당신 애가 답일지 몰라요.
 

 We should sacrifice her.
 성제무두 애를 희생해야 해요.
 

 She's my daughter. 마틸다는 기미 딸이고.
 She's coming with me. 나랑 아울러 갈거예요.
 

 You be strong, okay?
 약해지지 마

 

 We're not giving up.
 포기하면 빙처 돼
 

 -Get on the ground! 바닥에 엎드려
 -No one is attacking us! 아무도 우린 못 건드려!
 

 Grenade!
 수류탄이다!
 

 Our survival depends on her.
 우리 생사가 젓가락 애한테 달렸어

 

 Let's get to work.
 시작해 볼까?
